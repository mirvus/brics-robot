# -*- coding: utf-8 -*-
from time import sleep
from sys import stdout, stderr, stdin
#import numpy

from move_picker import MovePicker
#available strategies
from move_picker import _SIMPLE, _RANDOM, _GAME_TREE, _ISOLATED_CELLS

def make_move(array, m, v):
	ints = [int(i) - 1 for i in m.split()]
	array[ints[1]][ints[0]] = v
	array[ints[3]][ints[2]] = v

def play_game(movePicker):
	ping = input()
	print("PONG")
	stdout.flush()
	board_size = int(input())

	array = [[ 0 for i in range(board_size)] for j in range(board_size)]
	#array = numpy.zeros((board_size, board_size), numpy.uint8)

	first = input()
	if first.upper() == "ZACZYNAJ":
		m = movePicker.pick_next_move(array, None)
		make_move(array, m, 1)
		print(m)
	else:
		m = first
		make_move(array, m, 2)
		m = movePicker.pick_next_move(array, m)
		make_move(array, m, 1)
		print(m)
	while True:
		m = input()
		sleep(0.001)
		make_move(array, m, 2)
		m = movePicker.pick_next_move(array, m)
		make_move(array, m, 1)
		print(m)



movePicker = MovePicker(strategy=_GAME_TREE, tree_cutoff=14, make_log=True)
play_game(movePicker)
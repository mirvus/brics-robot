# -*- coding: utf-8 -*-
import copy
from board_utils import *

from time import sleep
from fun_timeout import timeout
from multiprocessing.context import TimeoutError

def simulate_move(array, move, player):
	new_array = copy.deepcopy(array)
	cords = [int(i) - 1 for i in move.split(' ')]
	new_array[cords[1]][cords[0]] = player
	new_array[cords[3]][cords[2]] = player
	return new_array


class Node(object):
	def __init__(self, move):
		self.move = move
		self.score = float('-inf')
		self.children = []

	def add_child(self, obj):
		self.children.append(obj)

	def get_move(self):
		return self.move

	def get_score(self):
		return self.score

	def __str__(self):
		return "Node(" + str(self.move) + ", " + str(round(self.score,3)) + ")"

	def __repr__(self):
		return "(" + str(self.move) + ", " + str(self.score) + ")"


class GameTree(object):
	def __init__(self, root):
		self.root = root

	def display(self, node, depth=0):
		children = node.children
		if depth == 0:
			print("{0}".format(node))
		else:
			print("\t"*depth, "{0}".format(node))
		
		depth += 1
		for child in children:
			self.display(child, depth)

	def add_all_children(self, array, move, parent):
		player = 1
		array = simulate_move(array, move, player)
		pos_moves = possible_moves(array)
		for move in pos_moves:
			cur = Node(move)
			parent.add_child(cur)
			self.add_all_children(array, move, cur)

	def best_move(self):
		best_move = None
		best_score = float('-inf')
		for node in self.root.children:
			if node.score > best_score:
				best_move = node.move
				best_score = node.score
		return (best_move, best_score)

	def all_child_scored(self):
		for child in self.root.children:
			if child.score == float("-inf"):
				return False
		return True

	def update_tree_score(self):
		"""Updates tree score.
		Uses bottom up score filling with max_tree_depth update_score calls. Inefficient."""
		while not self.all_child_scored():
			self.update_score(self.root)


	def update_score(self, parent, depth=0):
		"""Updates score. Uses recursive calls"""
		children = parent.children
		depth += 1
		for node in children:
			if not node.children: # no children - 0 if losing, 1 winning
				node.score = float(depth % 2) # even depth means lose (enemy lays last brick)
			else:
				# if it has children score is average of children scores
				scores = [x.get_score() for x in node.children]
				node.score = sum(scores) / len(scores)
			self.update_score(node, depth)


@timeout(0.8)
def fill_tree_timeout(array, tree, prev, root):
	tree.add_all_children(array, prev, root)
	tree.update_tree_score()
	return tree

def create_game_tree(array, prev):
	root = Node(prev)
	tree = GameTree(root)
	try:
		fill_tree_timeout(array, tree, prev, root)
	except TimeoutError:
		return tree

	#tree.display(root)
	return tree


def game_tree_move(array, prev):
	array_dup = copy.deepcopy(array)
	tree = create_game_tree(array_dup, prev)
	move, score = tree.best_move()
	return (move, score)

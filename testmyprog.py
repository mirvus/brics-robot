# -*- coding: utf-8 -*-
from board_utils import *
from game_tree import *
import time
import copy

def arr2str(array):
	res = ""
	for row in array:
		for cell in row:
			res = res + str(cell) + " "
		res += "\n" 
	return res




size = 5
array = read_array('board_state_5x5.txt')

start = time.time()

print(arr2str(array))
array_dup = copy.deepcopy(array)
prev_move = "1 1 1 2"
tree = create_game_tree(array_dup, prev_move)
move, score = tree.best_move()
tree.display(tree.root)


stop = time.time()
fun_time = round((stop - start), 3)
print("Move: ", move, ", time: ", fun_time)

# -*- coding: utf-8 -*-
#import numpy

def is_cell_isolated(arr, size, x, y):
	"""Returns true if cell is isolated i.e. placing brick on that cell is impossible."""

	is_isolated = True
	# cell is not empty, treat as isolated
	if arr[x][y] != 0: 
		return True
	if x+1 < size and arr[y][x+1] == 0:
		is_isolated = False
	if y+1 < size and arr[y+1][x] == 0:
		is_isolated = False
	if x-1 >= 0 and arr[y][x-1] == 0:
		is_isolated = False
	if y-1 >= 0 and arr[y-1][x] == 0:
		is_isolated = False
	return is_isolated


def tup2str(tup):
	return str(tup[0]) + " " + str(tup[1]) + " " + str(tup[2]) + " " + str(tup[3])

def transpose(array):
	return [[row[i] for row in array] for i in range(len(array[0]))]


def simple_move(array):
	"""Returns simple move e.g. first valid one"""
	move = None
	VERTICAL = 1
	for orientation, arr in enumerate( (array, transpose(array))):
		for y, row in enumerate(arr):
			for x in range(1, len(row)):
				tile = (row[x-1], row[x])
				if tile == (0, 0):
					if orientation is VERTICAL: #vertical brics, matrix transposed
						move_com = tup2str((y+1, x, y+1, x+1))
						return move_com
					else: # horizontal brics, matrix not transposed
						move_com = tup2str((x, y+1, x+1, y+1))
						return move_com

def possible_moves(array):
	"""Given current board state return array of all possible moves.
	Each move is in format x_1, y_1, x_2, y_2"""
	pos_moves = []
	VERTICAL = 1
	for orientation, arr in enumerate( (array, transpose(array))):
		for y, row in enumerate(arr):
			#print(row)
			for x in range(1, len(row)):
				tile = (row[x-1], row[x])
				#print(tile)
				if tile == (0, 0):
					if orientation is VERTICAL: #vertical brics, matrix transposed
						move_com = tup2str((y+1, x, y+1, x+1))
						pos_moves.append(move_com)
					else: # horizontal brics, matrix not transposed
						move_com = tup2str((x, y+1, x+1, y+1))
						pos_moves.append(move_com)
	return pos_moves

def unique_cells(pos_moves):
	"""Returns set of unique cells that can be used to place brick.
	It is not added if cell is isolated e.g.:
		  1 2 3
		  _ _ _
	   1 |0 B B
	   2 |A A
	   
	   cell (1, 1) is isolated."""  
	unique_cells = set()
	for move in pos_moves:
		x_1, y_1, x_2, y_2 = move.split(' ')
		unique_cells.add((int(x_1), int(y_1)))
		unique_cells.add((int(x_2), int(y_2)))
	return unique_cells

def num_unique_cells(unique_cells):
	"""Returns length of set of unique cells"""
	return len(unique_cells)

def read_array(filename):
	"""Returns array read from file. None if file does not exist."""
	with open(filename, 'r') as f:
		data = f.read().split('\n')
		size = int(data[0])
		array = []
		for row in data[1:]:
			cells = [int(i) for i in row.split(' ')]
			array.append(cells)
		#return numpy.array(array)
		return array



def brick_isolated():
	"""Returns first brick that isolates surrounding brics.
		B -brick, S - brick surroundings, D - possible to deny

		Placing brick B may result in making cells S isolated
		 - - - - - -
		|D D D D D D|
	    |D S S S S D| 
	    |D S B B S D|
	    |D S S S S D|
	   	|D D D D D D|
		 - - - - - -
	"""
	raise NotImplementedError
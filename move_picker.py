# -*- coding: utf-8 -*-
import random
import logging

from board_utils import *
from game_tree import game_tree_move

_SIMPLE, _RANDOM, _GAME_TREE, _ISOLATED_CELLS = range(4)

class MovePicker(object):
	def __init__(self, strategy=_GAME_TREE, tree_cutoff= 8, make_log=False):
		self.strategy = strategy
		self.make_log = make_log
		self.tree_cutoff = tree_cutoff
		if make_log:
			logging.basicConfig(filename='game_debug.log',level=logging.DEBUG,
								format='%(asctime)s %(message)s')
			logging.info("\n\n---New game---")

	def pick_next_move(self, array, prev):
		"""Picks next move according to current strategy
			move is in format 'x_1 y_1 x_2 y_2' """
		if self.strategy == _SIMPLE:
			return self.simple_move(array)
		if self.strategy == _RANDOM:
			return self.random_move(array)
		elif self.strategy == _ISOLATED_CELLS:
			raise NotImplementedError
		elif self.strategy == _GAME_TREE:
			pos_moves = possible_moves(array)
			if len(pos_moves) <= self.tree_cutoff:
				result = game_tree_move(array, prev)
				is_tree_move = False
				move, score = (None, None)
				if result[0] is not None:
					move, score = result
					is_tree_move = True
				if move is None: # move not found in time, pick random
					move = self.simple_move(array)
				# for game log
				if self.make_log:
					unique = len(pos_moves)
					if is_tree_move:
						logging.info(" Game Tree strategy, move: " + str(move) + \
									 ", score: " + str(round(score, 3)) + \
									 ", cells left: " + str(unique))
					else:
						logging.info(" Game Tree strategy, time exceeded." + \
									 " Simple move: " + str(move) + ", cells: " + str(unique))
				return move
			else:
				move = self.simple_move(array)
				if self.make_log:
					logging.info(" Simple strategy, move: " + str(move))
				return move

	def simple_move(self, array):
		"""Picks first valid move"""
		move = simple_move(array)
		return move

	def random_move(self, array):
		"""Picks random move"""
		pos_moves = possible_moves(array)
		move = random.choice(pos_moves)
		return move

	def isolated_cells_move(self, array):
		"""Isolated cells strategy:
		 -count no of unique cells (i.e. unisolated)
		 -if uneven, pick deny_1 move or set up deny_1 move
		 -if even, pick standard move"""
		pos_moves = possible_moves(array)
		unique = len(unique_cells(pos_moves))
		if (unique % 2) == 0:
			move = self.no_isolated_move(array, pos_moves)
		elif (unique % 2) != 0:
			move = self.deny_1_cell_move(array, pos_moves)
		if self.make_log:
			logging.info(" Unique cells:" + str(unique))
		return move

	def no_isolated_move(self, array, pos_moves):
		"""Returns move that does not create isolated cell"""
		# analize first move from pos_moves
		# if move correct return it
		# if incorrect, try next move
		raise NotImplementedError

	def deny_1_cell_move(self, array, pos_moves):
		"""Returns move that creates isolated cell.
		If such move is impossible, return None"""
		raise NotImplementedError
